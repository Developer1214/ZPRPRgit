#include<stdio.h>
#include<stdlib.h>
#include<regex.h>
#include<string.h>


int getYear(char *str)
{
    int year;
    char *token;
    token = strtok(str, "/");
    year = atoi(token);
    return year;
}

void numberAnalayzer(char *inputStr, float *inputMax, float *inputMin, float *inputAvg,int *inputTestNum){
    float bally[6];
    float max = 0.0;
    float min = 0.0;
    float avg = 0.0;
    int testNum = 1;
    sscanf(inputStr, "%f;%f;%f;%f;%f;%f", &bally[0], &bally[1], &bally[2], &bally[3], &bally[4], &bally[5]);
    avg = (bally[0] + bally[1] + bally[2] + bally[3] + bally[4] + bally[5])/6;
    for(int i = 0;i<6;i++){
        if(bally[i] > max){
            max = bally[i];
            testNum = i + 1;
        }
        if(bally[i] < min || min == 0){
            min = bally[i];
        }
    }
    *inputTestNum = testNum;
    *inputMax = max;
    *inputMin = min;
    *inputAvg = avg;
    return;
}


char * getPriezvisko(char *inputStr){
    char inputmeno[strlen(inputStr)];
    strcpy(inputmeno, inputStr);
    char * one = strtok(inputmeno, " ");
    char * two = strtok(NULL, " ");
    char * three = strtok(NULL, " ");
    if(three == NULL){
        return two;
    } else {
        return three;
    }
}

char * chekGender(char *inputStr){
    if(strcmp(inputStr,"m") == 0){
            inputStr = "muz";
        } else if(strcmp(inputStr,"f") == 0){
            inputStr = "zena";
        }
    return inputStr;
}

int checkStructure(FILE *file, char *inputStr){
    char str[200];
    regex_t regex;
    char *pattern = "^[a-zA-Z -]{4,49};[f|m];[0-9]{4}/((1[0-2])|0[1-9])/((0[1-9])|([1-2][0-9])|3[0-1]);[a-z]{1,}(;([0-9]{1,2}.[0-9]{2}|100.00)){6}(\n|)";
    int regc = regcomp(&regex, pattern, REG_EXTENDED);
    if(regc != 0){
        return 1;
    }
    if(file == NULL && inputStr != NULL){
        if(regexec(&regex, inputStr, 0, NULL, 0) == 0){
            return 0;
        } else {
            return 1;
        }
    } else if(file != NULL && inputStr == NULL){
        while(fgets(str, 200, file) != NULL){
            if(regexec(&regex, str, 0, NULL, 0) != 0)
            {   
                return 1;
            }
        }
        fseek(file, 0, SEEK_SET);
        return 0;
    }else {
        return 1;
    }
}

FILE *openFile(char * comm){
    if(comm == NULL){
        return NULL;
    }
    FILE *fp;
    fp = fopen("studenti.csv", comm);
    
    if(fp == NULL){
        printf("Subor nie je mozne precitat\n");
        fclose(fp);
        return NULL;
    }
    if(checkStructure(fp,NULL) == 1){
        printf("Subor nie je mozne precitat\n");
        fclose(fp);
        return NULL;
    }
    return fp;
}


void sum(){
    char str[200];
    FILE *fp = openFile("r");
    if(fp == NULL){
        return;
    }
    while(fgets(str,200,fp) != NULL ){
        char meno[50];
        char gender[5];
        char rok[12];
        char mesto[50];
        char bally[150];
        sscanf(str,"%[^;];%[^;];%[^;];%[^;];%[^\n]",meno,gender,rok,mesto,bally);
        strcpy(gender,chekGender(gender)); 
        int resRok = getYear(rok);
        printf("%s, nar. %d, %s, Kraj: %s\n", meno, resRok, gender, mesto);
        printf("Vysledky testov: %s\n", bally);

    }
    fclose(fp);
}

void who(){
    FILE *fp = openFile("r");
    if(fp == NULL){
        return;
    }
    char input[50];
    printf("Zadajte priezvisko studenta: ");
    scanf("%49s", input);

    char str[200];
    int checker = 0;
    int testNum = 0;

    while(fgets(str,200,fp) != NULL ){
        char meno[50];
        char gender[5];
        char rok[12];
        char mesto[50];
        char bally[150];
        float max,min,avg;
        sscanf(str,"%[^;];%[^;];%[^;];%[^;];%[^\n]",meno,gender,rok,mesto,bally);
        char priezvisko[30];
        int year = getYear(rok);
        strcpy(priezvisko,getPriezvisko(meno));
        
        if(strcmp(priezvisko,input) != 0){
            continue;
        }
        checker++;
        strcpy(gender,chekGender(gender));
        printf("%s\n",meno);
        printf("nar. %s, %s\n", rok, gender); 
        printf("Kraj: %s\n\n", mesto);
        
        printf("Vysledky testov: %s\n\n", bally);
    
        numberAnalayzer(bally,&max,&min,&avg,&testNum);

        printf("Najlepsi test: %.2f\n", max);
        printf("Najhorsi test: %.2f\n", min);
        printf("Priemerny test: %.2f\n", avg);
        break;
    }
    if(checker == 0){
        printf("Student s priezviskom %s sa nenasiel\n", input);
    }
    fclose(fp);
}

void best(){
    char str[200];
    FILE *fp = openFile("r");
    if(fp == NULL){
        return;
    }
    char bestName[50];
    float bestTest = 0;
    int bestTestNum = 1;
    while(fgets(str,200,fp) != NULL ){
        float max,min,avg;
        char meno[50];
        char gender[5];
        char rok[12];
        char mesto[50];
        char bally[150];
        int testNum = 0;
        sscanf(str,"%[^;];%[^;];%[^;];%[^;];%[^\n]",meno,gender,rok,mesto,bally);
        numberAnalayzer(bally,&max,&min,&avg,&testNum);
        if(max > bestTest){
            bestTest = max;
            strcpy(bestName,meno);
            bestTestNum = testNum;
        }
    }
    printf("Najelepsi test: %.2f\n", bestTest);
    printf("Student: %s\n", bestName);
    printf("Cislo testu: %d\n", bestTestNum);
    fclose(fp);
}

void region(){
    printf("Not working yet\n");
}

void year(){
    char str[200];
    FILE *fp = openFile("r");
    if(fp == NULL){
        return;
    }
    double bestTest = 0;
    char bestName[50];
    int bestYear;
    int getYear;
    int counter = 0;
    printf("Zadajte rok: ");
    scanf("%4d", &getYear);

    int testNum = 0;
    while(fgets(str,200,fp) != NULL ){
        float max,min,avg;
        char *meno = strtok(str, ";");
        strtok(NULL, ";");
        char *rok = strtok(NULL, "/");strtok(NULL, ";");
        strtok(NULL, ";");
        char *bally = strtok(NULL, "\n");
        numberAnalayzer(bally,&max,&min,&avg,&testNum);
        if(max > bestTest && atoi(rok) > getYear){
            counter++;
            bestTest = max;
            strcpy(bestName,meno);
            bestYear = atoi(rok);
        }


    }
    if (counter == 0){
        printf("Neni v zozname student narodeneny skor ako %d\n",getYear);
    } else{
        printf("Student: %s\n", bestName);
        printf("nar. %d\n", bestYear);
        printf("Najelepsi test: %.2f\n", bestTest);
        printf("Cislo testu: %d\n", testNum);
    }
    fclose(fp);
}

void avarage(){
    char str[200];
    FILE *fp = openFile("r");
    if(fp == NULL){
        return;
    }
    while(fgets(str,200,fp) != NULL ){
        char meno[50];
        char gender[5];
        char rok[12];
        char mesto[50];
        char bally[150];
        float avg;
        sscanf(str,"%[^;];%[^;];%[^;];%[^;];%[^\n]",meno,gender,rok,mesto,bally);
        numberAnalayzer(bally,0,0,&avg,0);

   
    }

}

void gender(){
    char str[200];
    FILE *fp = openFile("r");
    if(fp == NULL){
        return;
    }
    double bestTest = 0;
    char bestName[50];
    char inputGender[2];
    printf("Zadajte pohlavie: ");
    scanf("%1s", inputGender);
    if(strcmp(inputGender,"f") != 0 && strcmp(inputGender,"m") != 0){
        printf("Zadali ste nespravne pohlavie\n");
        fclose(fp);
        return;
    }
    int testNum;
    while(fgets(str,200,fp) != NULL ){
        float max,min,avg;
        char *meno = strtok(str, ";");
        char *gender = strtok(NULL, ";");
        strtok(NULL, ";");
        strtok(NULL, ";");
        char *bally = strtok(NULL, "\n");
        numberAnalayzer(bally,&max,&min,&avg,&testNum);
        if(max > bestTest && strcmp(inputGender,gender) == 0){
            bestTest = max;
            strcpy(bestName,meno);
        }
    }
    printf("Najelepsi test: %.2f\n", bestTest);
    printf("Student: %s\n", bestName);
    printf("Cislo testu: %d\n", testNum);
    fclose(fp);
}


void change(){
    FILE *fp = openFile("r+");
    if(fp == NULL){
        return;
    }
    char str[250];
    char input[30];
    int testNum;
    int counter = 0;
    char newResult[7];
    printf("Zadajte priezvisko studenta, poradove cislo testu, novy vysledok: ");
    scanf(" %29s %d %6s", input, &testNum, newResult);
    if(testNum < 1 || testNum > 6){
        printf("Zadali ste cislo testu %d, musi byt od 1 do 6\n", testNum);
        fclose(fp);
        return;
    }
    regex_t regex;
    int reg = regcomp(&regex, "(^([0-9]?[0-9].[0-9][0-9])|(100.00))", REG_EXTENDED);
    if(reg != 0){
        printf("Chyba pri kompilacii regularneho vyrazu\n");
        fclose(fp);
        return;
    }
    if(regexec(&regex, newResult, 0, NULL, 0) != 0){
        printf("Zadali ste nespravny vysledok testu\n");
        fclose(fp);
        return;
    }
    char meno[70],pol[2],rok[11],mesto[50];
    char bally1[8],bally2[8],bally3[8],bally4[8],bally5[8],bally6[8];
    char tmpName[] = ".studenti_tmp.csv";
    FILE *tmp = fopen(tmpName,"w+");
    while(fgets(str,200,fp) != NULL) {
        sscanf(str, "%[^;];%[^;];%[^;];%[^;];%[^;];%[^;];%[^;];%[^;];%[^;];%s", meno, pol, rok, mesto, bally1, bally2, bally3, bally4, bally5, bally6);
        char priezvisko[30];
        strcpy(priezvisko,getPriezvisko(meno));
        if(strcmp(priezvisko,input) == 0) {
            counter++;
            switch (testNum) {
                case 1:
                    strcpy(bally1, newResult);
                    break;
                case 2:
                    strcpy(bally2, newResult);
                    break;
                case 3:
                    strcpy(bally3, newResult);
                    break;
                case 4:
                    strcpy(bally4, newResult);
                    break;
                case 5:
                    strcpy(bally5, newResult);
                    break;
                case 6:
                    strcpy(bally6, newResult);
                    break;
            }
        }
        sprintf(str, "%s;%s;%s;%s;%s;%s;%s;%s;%s;%s\n", meno, pol, rok, mesto, bally1, bally2, bally3, bally4, bally5, bally6);
        fprintf(tmp, "%s", str);
    }

    fseek(tmp, -1, SEEK_CUR);
    unsigned char eof = 0x00;
    fputc(eof, tmp);
    if(counter == 0){
        printf("Studenta s takymto priezviskom nemame\n");
        fclose(fp);
        fclose(tmp);
        remove(tmpName);
        return;
    }
    rewind(tmp);
    while(fgets(str,200,tmp) != NULL ){
        char meno[50];
        char gender[5];
        char rok[12];
        char mesto[50];
        char bally[150];
        sscanf(str,"%[^;];%[^;];%[^;];%[^;];%[^\n]",meno,gender,rok,mesto,bally);
        strcpy(gender,chekGender(gender)); 
        int resRok = getYear(rok);
        printf("%s, nar. %d, %s, Kraj: %s\n", meno, resRok, gender, mesto);
        printf("Vysledky testov: %s\n", bally);
    }
    fclose(tmp);
    fclose(fp);
    remove("studenti.csv");
    rename(tmpName,"studenti.csv");
    
}

void newstudent(){
    FILE *fp = openFile("a+");
    if(fp == NULL){
        return;
    }
    char str[200];
    char meno[50];
    char year[12];
    char pohlavie[2];
    char kraj[51];
    char bally[37];
    printf("Zadajte meno studenta: ");
    scanf(" %49[^\n]", meno);
    printf("Zadajte datum narodenia: ");
    scanf(" %11s", year);
    printf("Zadajte pohlavie: ");
    scanf(" %1s", pohlavie);
    printf("Zadajte kraj: ");
    scanf(" %50s", kraj);
    printf("Zadajte vysledky testov: ");
    scanf(" %36s", bally);
    sprintf(str,"%s;%s;%s;%s;%s",meno,pohlavie,year,kraj,bally);
    if(checkStructure(NULL,str) == 1){
        printf("Zadali ste nespravny format\n");
        fclose(fp);
        return;
    }

    fprintf(fp,"\n%s;%s;%s;%s;%s",meno,pohlavie,year,kraj,bally);
    rewind(fp);
    while(fgets(str,200,fp) != NULL ){
        char meno[50];
        char gender[5];
        char rok[12];
        char mesto[50];
        char bally[150];
        sscanf(str,"%[^;];%[^;];%[^;];%[^;];%[^\n]",meno,gender,rok,mesto,bally);
        strcpy(gender,chekGender(gender)); 
        int resRok = getYear(rok);
        printf("%s, nar. %d, %s, Kraj: %s\n", meno, resRok, gender, mesto);
        printf("Vysledky testov: %s\n", bally);

   
    }
    fclose(fp);
}

void delstudent(){
    char str[200];
    FILE *fp = openFile("r");
    if(fp == NULL){
        return;
    }
    char priezvisko[50];
    char tmpFileName[] = ".studenti_tmp.csv";
    printf("Zadajte prizvesko studenta: ");
    scanf(" %49[^\n]", priezvisko);
    FILE *tmp = fopen(tmpFileName,"w");
    if(tmp == NULL){
        printf("Nepodarilo sa urobit tmp subor\n");
        fclose(fp);
        fclose(tmp);
        return;
    }
    int counter = 0;
    while(fgets(str,200,fp) != NULL){
        char meno[50];
        sscanf(str,"%[^;]s",meno);
        char *priezvisko1 = getPriezvisko(meno);
        if(strcmp(priezvisko,priezvisko1) == 0){
            
            printf("Student s menom “%s“ bol vymazany.\n",meno);
            counter++;
            continue;
        }
        fprintf(tmp,"%s",str);
    }
    fseek(tmp, -1, SEEK_CUR);
    unsigned char eof = 0x00;
    fputc(eof, tmp);
    fclose(fp);
    fclose(tmp);
    if(counter == 0){
        printf("Student s takymto priezviskom neexistuje\n");
        remove(tmpFileName);
        return;
    }
    remove("studenti.csv");
    rename(tmpFileName,"studenti.csv");
}

void getCommand()
{

    while(1){
        char c;
        printf("Zadajte prikaz: ");
        scanf(" %c",&c);
        if(c == '\n'){
            continue;
        }
        switch(c)
        {
            case 's':
                sum();
                //Funkcia sum
                break;
            case 'w':
                who();
                //Funkcia who
                break;
            case 'b':
                best();
                //Funkcia best
                break;
            case 'g':
                gender();
                //Funkcia gender
                break;
            case 'r':
                region();
                //Funkcia region
                break;
            case 'y':
                year();
                //Funkcia year
                break;
            case 'a':
                avarage();
                //Funkcia avarage
                break;
            case 'o':
                //Funkcia over
                break;
            case 'c':
                change();
                //Funkcia change
                break;
            case 'n':
                newstudent();
                //Funkcia new student
                break;
            case 'd':
                delstudent();
                //Funkcia delete student
                break;
            case 'x':
                printf("Exit\n");
                exit(0);
                break;
            default:
                printf("Neznamy prikaz\n");
                break;
        }
    }
}
int main(){
    getCommand();
    return 0;
}


